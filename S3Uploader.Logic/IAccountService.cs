﻿using System.Collections.Generic;
using S3Uploader.Logic.Models;

namespace S3Uploader.Logic
{
    public interface IAccountService
    {
        List<AccountSetting> GetList();

        AccountSetting GetSelectedAccount();

        void Save(List<AccountSetting> settings);

        bool IsReady();
    }
}
