﻿using System;
using System.Threading.Tasks;

namespace S3Uploader.Logic.Extensions
{
    public static class TaskExtension
    {
        public static async Task<TResult> Map<TSource, TResult>(this Task<TSource> src, Func<TSource, TResult> func)
        {
            return func(await src.ConfigureAwait(false));
        }
    }
}
