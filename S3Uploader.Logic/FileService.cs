﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AmazonStorage.Contract.Factory;
using AmazonStorage.Contract.Models;
using AmazonStorage.Contract.Service;
using S3Uploader.Logic.Extensions;
using S3Uploader.Logic.Models;

namespace S3Uploader.Logic
{
    public class FileService : IFileService
    {
        private readonly IAmazonStorageFactory _amazonStorageFactory;
        private readonly IAccountService _accountService;

        public FileService(IAmazonStorageFactory amazonStorageFactory, IAccountService accountService)
        {
            _amazonStorageFactory = amazonStorageFactory;
            _accountService = accountService;
        }

        private IAmazonStorageService AmazonStorageService
        {
            get
            {
                var setting = _accountService.GetSelectedAccount();
                return _amazonStorageFactory.Create(new AmazonStorageConfig(setting.AccessKeyId, setting.SecretAccessKeyId));
            }
        }

        public async Task<List<string>> GetBucketListAsync(CancellationToken token)
        {
            if (!_accountService.IsReady())
            {
                return null;
            }

            return await AmazonStorageService.ListBucketAsync(token).ConfigureAwait(false);
        }

        public async Task IsPathsExistsAsync(List<string> bucketsName, string foldersPrefix, Action<Task<FolderFilterResult>> onIsExistsReceiveAsync, CancellationToken token)
        {
            if (!_accountService.IsReady())
            {
                return;
            }

            await AmazonStorageService.IsFilesExistsAsync(bucketsName, foldersPrefix, onIsExistsReceiveAsync, token).ConfigureAwait(false);
        }

        public async Task<List<string>> IsPathsExistsAsync(List<string> bucketsName, string foldersPrefix, CancellationToken token)
        {
            if (!_accountService.IsReady())
            {
                return null;
            }
            return await AmazonStorageService.IsFilesExistsAsync(bucketsName, foldersPrefix, token).ConfigureAwait(false);
        }

        public async Task GetFileListAsync(string bucketName, string prefix, Action<Task<List<FileList>>, bool> onBunchReceiveAsync, CancellationToken token)
        {
            if (!_accountService.IsReady())
            {
                return;
            }

            await AmazonStorageService.ListFilesAsync(bucketName, prefix, (task, isTruncated) =>
                {
                    onBunchReceiveAsync(task.Map(Convert), isTruncated);
                }, token)
                .ConfigureAwait(false);
        }

        public async Task UploadAsync(UploadModel model, CancellationToken token)
        {
            var filesForUpload = new List<FileForUpload>();
            foreach (var fileName in model.SelectedFilePaths)
            {
                var contentType = MimeMapping.MimeUtility.GetMimeMapping(fileName);

                var key = model.UploadPath + Path.GetFileName(fileName);

                filesForUpload.Add(new FileForUpload()
                {
                    UseSsl = model.UseSsl,
                    Key = key,
                    NoneCriticalData = model.NoneCriticalData,
                    IsPublic = model.IsPublic,
                    ContentType = contentType,
                    FileName = fileName,
                });
            }

            await AmazonStorageService.UploadFilesAsync(filesForUpload, model.UploadBuckets, token).ConfigureAwait(false);

            if (model.SendInvalidationRequest)
            {
                var keys = filesForUpload.Select(o => o.Key).ToList();
                await AmazonStorageService.InvalidationRequestAsync(keys, model.UploadBuckets, token).ConfigureAwait(false);
            }
        }

        private List<FileList> Convert(S3DirectoryInfo info)
        {
            var result = new List<FileList>();
            if (info.S3Folders.Any())
            {
                result.AddRange(info.S3Folders.Select(o => new FileList()
                {
                    Key = o.FolderName,
                    IsFolder = true
                }).ToList());
            }

            if (info.S3Objects.Any())
            {
                result.AddRange(info.S3Objects.Select(o => new FileList()
                {
                    LastModified = o.LastModified,
                    Key = o.Key,
                    Size = o.Size
                }).ToList());
            }

            return result;
        }

    }
}
