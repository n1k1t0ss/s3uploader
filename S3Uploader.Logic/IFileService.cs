﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AmazonStorage.Contract.Models;
using S3Uploader.Logic.Models;

namespace S3Uploader.Logic
{
    public interface IFileService
    {
        Task<List<string>> GetBucketListAsync(CancellationToken token);

        Task IsPathsExistsAsync(List<string> bucketsName, string foldersPrefix, Action<Task<FolderFilterResult>> onIsExistsReceiveAsync,
            CancellationToken token);
        Task<List<string>> IsPathsExistsAsync(List<string> bucketsName, string foldersPrefix, CancellationToken token);


        Task GetFileListAsync(string bucketName, string prefix, Action<Task<List<FileList>>, bool> onBunchReceiveAsync, CancellationToken token);

        Task UploadAsync(UploadModel model, CancellationToken token);
    }
}
