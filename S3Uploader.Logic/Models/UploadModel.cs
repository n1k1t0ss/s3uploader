﻿using System.Collections.Generic;

namespace S3Uploader.Logic.Models
{
    public class UploadModel
    {
        public List<string> SelectedFilePaths { get; set; }
        public string UploadPath { get; set; }
        public List<string> UploadBuckets { get; set; }
        public bool NoneCriticalData { get; set; }
        public bool UseSsl { get; set; }
        public bool SendInvalidationRequest { get; set; }
        public bool IsPublic { get; set; } = true;
    }
}
