﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace S3Uploader.Logic.Models
{
    public class FileList
    {
        public string Key { get; set; }
        public DateTime? LastModified { get; set; }
        public long? Size { get; set; }

        public bool IsFolder { get; set; } = false;

        public string FileName => IsFolder ? Key : Path.GetFileName(Key);
        public List<string> FilePaths 
        {
            get
            {
                var path = Path.GetDirectoryName(Key);
                return path?.Split(Path.DirectorySeparatorChar).ToList();
            }
        }
    }
}
