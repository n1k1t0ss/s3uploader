﻿using S3Uploader.Logic.Enums;

namespace S3Uploader.Logic.Models
{
    public class AccountSetting
    {
        public string AccountName { get; set; }
        public string AccessKeyId { get; set; }
        public string SecretAccessKeyId { get; set; }
        public AccountSettingType Type { get; set; }
        public bool IsSelected { get; set; }

        public AccountSetting Copy()
        {
            return (AccountSetting)this.MemberwiseClone();
        }
    }
}
