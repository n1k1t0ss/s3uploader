﻿using AmazonStorage.Contract.Factory;
using AmazonStorage.Contract.Models;
using AmazonStorage.Contract.Service;
using AmazonStorage.Implementation.Service;

namespace AmazonStorage.Implementation.Factory
{
    public class AmazonStorageFactory : IAmazonStorageFactory
    {
        public IAmazonStorageService Create(AmazonStorageConfig config)
        {
            return new AmazonStorageService(config);
        }
    }
}
