﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using AmazonStorage.Contract.Models;
using AmazonStorage.Contract.Service;
using AmazonStorage.Implementation.Extensions;

namespace AmazonStorage.Implementation.Service
{
    public class AmazonStorageService : IAmazonStorageService
    {
        private const int UploadTimeout = 600000; // 10 minutes

        private AmazonStorageConfig Config { get; }

        public AmazonStorageService(AmazonStorageConfig config)
        {
            Config = config;
        }

        private AmazonS3Client GetClient(bool useSsl = false, int? timeoutMilliseconds = null)
        {
            var credentials = new BasicAWSCredentials(Config.AccessKeyId, Config.SecretAccessKeyId);
            var amazonS3Config = new AmazonS3Config
            {
                UseHttp = !useSsl,
                RegionEndpoint = string.IsNullOrWhiteSpace(Config.RegionEndpointSystemName)
                    ? RegionEndpoint.USEast1
                    : RegionEndpoint.GetBySystemName(Config.RegionEndpointSystemName)
            };

            if (timeoutMilliseconds.HasValue)
            {
                amazonS3Config.Timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds.Value);
            }
            
            return new AmazonS3Client(credentials, amazonS3Config);
        }

        private AmazonCloudFrontClient GetCloudFrontClient(int? timeoutMilliseconds = null)
        {
            var credentials = new BasicAWSCredentials(Config.AccessKeyId, Config.SecretAccessKeyId);
            var amazonS3Config = new AmazonCloudFrontConfig()
            {
                UseHttp = false,
                RegionEndpoint = string.IsNullOrWhiteSpace(Config.RegionEndpointSystemName)
                    ? RegionEndpoint.USEast1
                    : RegionEndpoint.GetBySystemName(Config.RegionEndpointSystemName)
            };

            if (timeoutMilliseconds.HasValue)
            {
                amazonS3Config.Timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds.Value);
            }

            return new AmazonCloudFrontClient(credentials, amazonS3Config);
        }


        public async Task<List<string>> ListBucketAsync(CancellationToken token)
        {
            var result = new List<string>();

            using (var client = GetClient())
            {
                ListBucketsResponse buckets;
                try
                {
                    buckets = await client.ListBucketsAsync(token).ConfigureAwait(false);
                }
                catch (AmazonServiceException ex)
                {
                    throw new AmazonS3Exception(ex);
                }
                result.AddRange(buckets.Buckets.Select(bucket => bucket.BucketName));
            }
            return result;
        }

        public async Task ListFilesAsync(string bucketName, string prefix, Action<Task<S3DirectoryInfo>, bool> onKeysBatchReceiveAsync, CancellationToken token)
        {
            prefix = ConvertExtensionToLower(prefix);
            var delimiter = @"/";

            using (var client = GetClient())
            {
                var request = new ListObjectsV2Request
                {
                    BucketName = bucketName,
                    Prefix = prefix,
                    Delimiter = delimiter,
                    MaxKeys = 100
                };

                // while list of objects is not truncated
                do
                {
                    // try to get response
                    try
                    {
                        token.ThrowIfCancellationRequested();
                        // list objects
                        var responseAsync = client.ListObjectsV2Async(request, token);
                        token.ThrowIfCancellationRequested();

                        onKeysBatchReceiveAsync(responseAsync.Map(r => new S3DirectoryInfo(
                            r.S3Objects.Select(o => new S3ObjectsInfo(o.Key, o.LastModified, o.Size)).ToList(),
                            r.CommonPrefixes.Select(o => new S3FoldersInfo(o)).ToList()
                        )), false);

                        var response = await responseAsync.ConfigureAwait(false);

                        // if response is truncated
                        if (response.IsTruncated)
                            request.ContinuationToken = response.NextContinuationToken;
                        else
                            request = null;
                    }
                    catch (AmazonServiceException ex)
                    {
                        throw new AmazonS3Exception(ex);
                    }
                } while (!ReferenceEquals(null, request));
            }
        }

        public async Task<bool> IsFileExistsAsync(string bucketName, string prefix, CancellationToken token)
        {
            using (var client = GetClient())
            {
                var request = new ListObjectsV2Request
                {
                    BucketName = bucketName,
                    Prefix = prefix,
                    MaxKeys = 1
                };

                try
                {
                    // list objects
                    var response = await client.ListObjectsV2Async(request, token).ConfigureAwait(false);
                    return response.S3Objects.Any() || response.CommonPrefixes.Any();
                }
                catch (AmazonServiceException ex)
                {
                    throw new AmazonS3Exception(ex);
                }
            }
        }

        public async Task IsFilesExistsAsync(List<string> bucketNames, string foldersPrefix, Action<Task<FolderFilterResult>> onIsExistsReceiveAsync, CancellationToken token, int workersCount = 8)
        {
            await Task.Run(() => Parallel.ForEach(bucketNames,
                new ParallelOptions { MaxDegreeOfParallelism = Math.Min(workersCount, bucketNames.Count) },
                 (bucketName) =>
                {
                    var isExistsTask =  IsFileExistsAsync(bucketName, foldersPrefix, token);
                    onIsExistsReceiveAsync(isExistsTask.Map(isExists => new FolderFilterResult(bucketName, isExists)));
                }), token).ConfigureAwait(false);
        }

        public async Task<List<string>> IsFilesExistsAsync(List<string> bucketNames, string foldersPrefix, CancellationToken token, int workersCount = 8)
        {
            var result = new List<string>();

            await Task.WhenAll(bucketNames.Select(async (bucketName) => {
                var isExistsTask = await IsFileExistsAsync(bucketName, foldersPrefix, token).ConfigureAwait(false);
                if (isExistsTask)
                {
                        result.Add(bucketName);
                }
            })).ConfigureAwait(false);

            return result;
        }

        private string ConvertExtensionToLower(string key)
        {
            var extension = Path.GetExtension(key);
            return string.IsNullOrEmpty(extension) ? key : Path.ChangeExtension(key, extension.ToLower());
        }

        /// <summary>
        /// Upload a new file to amazon or change an existing file
        /// </summary>
        public async Task UploadFileAsync(string bucketName, StreamForUpload fileForUpload, CancellationToken token)
        {
            using (var client = GetClient(fileForUpload.UseSsl, UploadTimeout))
            {
                var putObjectRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileForUpload.IsPublic ? S3CannedACL.PublicRead : S3CannedACL.BucketOwnerFullControl,
                    Key = fileForUpload.Key = ConvertExtensionToLower(fileForUpload.Key),
                    InputStream = fileForUpload.FileStream,
                    ContentType = fileForUpload.ContentType,
                    StorageClass = fileForUpload.NoneCriticalData
                        ? S3StorageClass.ReducedRedundancy
                        : S3StorageClass.Standard,
                };
                
                putObjectRequest.AutoCloseStream = false;
                fileForUpload.FileStream.Position = 0;

                try
                {
                    var response = await client.PutObjectAsync(putObjectRequest, token).ConfigureAwait(false);
                }
                catch (AmazonServiceException ex)
                {
                    throw new AmazonS3Exception(ex);
                }
            }
        }

        /// <summary>
        /// Upload all files to bucket
        /// </summary>
        public async Task UploadFilesAsync(List<FileForUpload> filesForUpload, string bucketName, CancellationToken token)
        {
            await Task.WhenAll(filesForUpload.Select(async (fileForUpload) =>
            {
                using (var stream = new FileStream(fileForUpload.FileName, FileMode.Open))
                {
                    await UploadFileAsync(bucketName, fileForUpload.ToStreamForUpload(stream), token).ConfigureAwait(false);
                }
                
            })).ConfigureAwait(false);
        }

        /// <summary>
        /// Upload each key to each bucket
        /// </summary>
        public async Task UploadFilesAsync(List<FileForUpload> filesForUpload, List<string> bucketNames, CancellationToken token)
        {
            var bucketsForUpload = bucketNames.ToList();
            var bucket = bucketsForUpload[0];
            await UploadFilesAsync(filesForUpload, bucket, token).ConfigureAwait(false);
            bucketsForUpload.Remove(bucket);
            if (bucketsForUpload.Any())
            {
                var filesForCopy = (from fileForUpload in filesForUpload
                    from bucketName in bucketsForUpload
                    select new FileForCopy()
                    {
                        BucketNameFrom = bucket,
                        KeyFrom = fileForUpload.Key,
                        BucketNameTo = bucketName,
                        KeyTo = fileForUpload.Key,
                        IsPublic = fileForUpload.IsPublic,
                        NoneCriticalData = fileForUpload.NoneCriticalData,
                        UseSsl = fileForUpload.UseSsl
                    }).ToList();
                await CopyFilesAsync(filesForCopy).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Copy group of files on s3
        /// </summary>
        public async Task CopyFilesAsync(List<FileForCopy> filesForCopy)
        {
            await Task.WhenAll(filesForCopy.Select(async (fileForCopy) =>
            {
                await CopyFileAsync(fileForCopy).ConfigureAwait(false);
            })).ConfigureAwait(false);
        }

        /// <summary>
        /// Copy file on s3
        /// </summary>
        public async Task CopyFileAsync(FileForCopy fileForCopy)
        {
            using (var client = GetClient(fileForCopy.UseSsl))
            {
                try
                {
                    var request = new CopyObjectRequest()
                    {
                        SourceBucket = fileForCopy.BucketNameFrom,
                        SourceKey = ConvertExtensionToLower(fileForCopy.KeyFrom),
                        DestinationBucket = fileForCopy.BucketNameTo,
                        DestinationKey = ConvertExtensionToLower(fileForCopy.KeyTo),
                        CannedACL = fileForCopy.IsPublic ? S3CannedACL.PublicRead : S3CannedACL.BucketOwnerFullControl,
                        StorageClass = fileForCopy.NoneCriticalData ? S3StorageClass.ReducedRedundancy : S3StorageClass.Standard
                    };

                    await client.CopyObjectAsync(request).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    ex.Data["SourceBucket"] = fileForCopy.BucketNameFrom;
                    ex.Data["SourceKey"] = ConvertExtensionToLower(fileForCopy.KeyFrom);
                    ex.Data["DestinationBucket"] = fileForCopy.BucketNameTo;
                    ex.Data["DestinationKey"] = ConvertExtensionToLower(fileForCopy.KeyTo);
                    throw ex;
                }
            }
        }

        private async Task<List<string>> GetDistributionIdsAsync(List<string> buckets, CancellationToken token)
        {
            using (var client = GetCloudFrontClient())
            {
                var distributionRequest = new ListDistributionsRequest();

                var distribution = await client.ListDistributionsAsync(distributionRequest, token)
                    .ConfigureAwait(false);

              return   distribution.DistributionList.Items
                    .Where(i => i.Origins.Items.Any(o => buckets.Select(bucket => $"{bucket}.s3.amazonaws.com").Contains(o.DomainName)))
                    .Select(o => o.Id)
                    .ToList();
            }
        }

        public async Task InvalidationRequestAsync(List<string> keys, List<string> buckets, CancellationToken token)
        {
            // get distribution ID
            var ditributionIds = await GetDistributionIdsAsync(buckets, token).ConfigureAwait(false);

            await Task.WhenAll(ditributionIds.Select(async (ditributionId) =>
            {
                await InvalidationRequestByDitributionIdAsync(keys, ditributionId, token).ConfigureAwait(false);
            })).ConfigureAwait(false);
        }

        private async Task InvalidationRequestByDitributionIdAsync(List<string> keys, string ditributionId, CancellationToken token)
        {
            // keys must starts from '/'
            keys = keys.Select(key => key.StartsWith(@"/") ? key : string.Concat(@"/", key)).Select(ConvertExtensionToLower).ToList();

            // send Invalidation request
            using (var client = GetCloudFrontClient())
            {
                await client.CreateInvalidationAsync(new CreateInvalidationRequest
                {
                    DistributionId = ditributionId,
                    InvalidationBatch = new InvalidationBatch
                    {
                        Paths = new Paths
                        {
                            Quantity = keys.Count,
                            Items = keys
                        },
                        CallerReference = DateTime.Now.Ticks.ToString()
                    }
                }, token)
                .ConfigureAwait(false);
            }
        }
    }
}
