﻿using System;
using System.Text;
using Amazon.Runtime;

namespace AmazonStorage.Implementation.Exceptions
{
    public class AmazonS3Exception : ApplicationException
    {
        internal AmazonS3Exception(AmazonServiceException exception)
            : base(BuildMessage(exception), exception) { }

        public AmazonS3Exception() { }

        private static string BuildMessage(AmazonServiceException exception)
        {
            var sb = new StringBuilder("Amazon S3 Exception: ");
            sb.Append(exception.Message).AppendLine();
            sb.AppendFormat("Request ID: {0}", exception.RequestId).Append(Environment.NewLine);
            sb.AppendFormat("Status Code: {0}", exception.StatusCode.ToString()).Append(Environment.NewLine);
            sb.AppendFormat("Error Type: {0}", exception.ErrorType.ToString()).Append(Environment.NewLine);
            sb.AppendFormat("Error Code: {0}", exception.ErrorCode).Append(Environment.NewLine);
            return sb.ToString();
        }
    }
}
