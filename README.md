## About application
I developed this application to automize some rutine work, that couldn't be done by using standart [amazon s3browser](https://s3browser.com). 
The main and only purpose is searching files and uploading to multiple amazon s3 buckets at the same time.
Imagine you want to upload new image to your production s3 server. If you have several environments - dev, qa, production and etc you may want to upload that image to all of them to synhronize them.
I didn't find how to do it using standart Amazon features (maybe rigth now they did something like this) That tools will help you to do this. 
Even if none of listed bucket have same "folders" - searching will remove them from a list.

## How to use it 
Add your s3 account as far as you do it in s3brouser. If you dind'n have it - you need to register it and create [account](https://portal.aws.amazon.com/billing/signup) 
I don't know, is it free or not. 
Then create several test buckets with folders and some files in it.
You will see list of buckets in s3Uploader at the left, at the right - list of files, that each bucket have (if you click on it).
Enter in filter edit part of searching file\path. 
Press enter - and list of buckets updates.
List of buckets and files show only filtered content. You could show folder content only throught filter. Yep, it's strange, wasn't time to do this feature.
You could upload file only to folder, so make sure path is ending with '/';

## About solution
This was the first attempt to build WPF application. I made it superquick and hadn't much time to organize solution and projects. Far more later I made some cosmethic changes, but not a bit.
As for now, I want to improve and refactoring this code, but I always have no time for this. 
So some decisions that was made here wasn't great, and I know it. But its the only code I could show to public.

I didn't use MVVM frameworks like PRISM and mvvmlight, because I wanted to explore MVVM world by myself, without framework help. 
I think, I found an interesting solution in this sutiation - ViewModels works as a controllers and could manipulate view without problems. 
The idea is to use IViewManager to call\get all states from Views. And IMainViewModel could be injected everywhere, so you could call everything you need - create View\ViewModel and handle everything in the project.
And there is no need to use events to transfer data between viewModels - it could be done by calling IMainViewModel
Also I had a great time writing async commands, with help of [Stephen Cleary's](https://blog.stephencleary.com) articles. 

PS. Yes, I know about IoC containers, it wasn't needed in such small project.


