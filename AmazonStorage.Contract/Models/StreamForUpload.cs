﻿using System.IO;

namespace AmazonStorage.Contract.Models
{
    public class StreamForUpload : ObjectForUpload
    {
        public Stream FileStream { get; set; }
    }
}
