﻿namespace AmazonStorage.Contract.Models
{
    public class S3FoldersInfo
    {
        public S3FoldersInfo(string folderName)
        {
            FolderName = folderName;
        }

        public string FolderName { get; set; }
    }
}
