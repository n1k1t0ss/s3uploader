﻿namespace AmazonStorage.Contract.Models
{
    public abstract class ObjectForUpload
    {
        /// <summary>
        /// amazon key
        /// </summary>
        public string Key { get; set; }

        public string ContentType { get; set; }
        /// <summary>
        /// if true - Owner gets FULL_CONTROL and the anonymous principal is granted READ access.
        /// If this policy is used on an object, it can be read from a browser with no authentication.
        /// </summary>
        public bool IsPublic { get; set; }
        /// <summary>
        /// http://aws.amazon.com/s3/faqs/#What_is_RRS
        /// </summary>
        public bool NoneCriticalData { get; set; }
        /// <summary>
        /// Use https when upload file
        /// </summary>
        public bool UseSsl { get; set; } = true;

    }
}
