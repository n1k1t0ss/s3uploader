﻿namespace AmazonStorage.Contract.Models
{
    public class FolderFilterResult
    {
        public bool IsExists { get; set; }
        public string BucketName { get; set; }

        public FolderFilterResult(string bucketName, bool isExists)
        {
            IsExists = isExists;
            BucketName = bucketName;
        }
    }
}
