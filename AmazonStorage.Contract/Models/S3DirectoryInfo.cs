﻿using System.Collections.Generic;

namespace AmazonStorage.Contract.Models
{
    public class S3DirectoryInfo
    {
        public IList<S3ObjectsInfo> S3Objects { get; set; }
        public IList<S3FoldersInfo> S3Folders { get; set; }

        public S3DirectoryInfo(IList<S3ObjectsInfo> s3Objects, IList<S3FoldersInfo> s3Folders)
        {
            S3Objects = s3Objects;
            S3Folders = s3Folders;
        }
    }
}
