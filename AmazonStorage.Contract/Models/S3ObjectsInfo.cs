﻿using System;

namespace AmazonStorage.Contract.Models
{
    public class S3ObjectsInfo
    {
        public S3ObjectsInfo(string key, DateTime lastModified, long size)
        {
            Key = key;
            LastModified = lastModified;
            Size = size;
        }

        public string Key { get; set; }
        public DateTime LastModified { get; set; }
        public long Size { get; set; }
    }
}
