﻿namespace AmazonStorage.Contract.Models
{
    public class AmazonStorageConfig
    {
        public AmazonStorageConfig(string accessKeyId, string secretAccessKeyId)
        {
            AccessKeyId = accessKeyId;
            SecretAccessKeyId = secretAccessKeyId;
        }

        public string AccessKeyId { get; set; }
        public string SecretAccessKeyId { get; set; }

        public string RegionEndpointSystemName { get; set; }
    }
}
