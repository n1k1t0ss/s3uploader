﻿using System.IO;

namespace AmazonStorage.Contract.Models
{
    public class FileForUpload : ObjectForUpload
    {
        /// <summary>
        /// file content
        /// </summary>
        public string FileName { get; set; }

        public StreamForUpload ToStreamForUpload(Stream stream)
        {
            return new StreamForUpload
            {
                FileStream = stream,
                ContentType = this.ContentType,
                UseSsl = this.UseSsl,
                Key = this.Key,
                IsPublic = this.IsPublic,
                NoneCriticalData = this.NoneCriticalData
            };
        }
    }
}
