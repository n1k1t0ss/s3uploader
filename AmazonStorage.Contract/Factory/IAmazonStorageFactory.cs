﻿using AmazonStorage.Contract.Models;
using AmazonStorage.Contract.Service;

namespace AmazonStorage.Contract.Factory
{
    public interface IAmazonStorageFactory
    {
        IAmazonStorageService Create(AmazonStorageConfig config);
    }
}
