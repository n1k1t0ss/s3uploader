﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AmazonStorage.Contract.Models;

namespace AmazonStorage.Contract.Service
{
    public interface IAmazonStorageService
    {
        Task<List<string>> ListBucketAsync(CancellationToken token);

        Task ListFilesAsync(string bucketName, string prefix, Action<Task<S3DirectoryInfo>, bool> onKeysBatchReceiveAsync, CancellationToken token);

        Task<bool> IsFileExistsAsync(string bucketName, string prefix, CancellationToken token);

        Task<List<string>> IsFilesExistsAsync(List<string> bucketNames, string foldersPrefix, CancellationToken token, int workersCount = 8);
        Task IsFilesExistsAsync(List<string> bucketNames, string foldersPrefix, Action<Task<FolderFilterResult>> onIsExistsReceiveAsync, CancellationToken token,
            int workersCount = 8);

        Task InvalidationRequestAsync(List<string> keys, List<string> buckets, CancellationToken token);

        Task UploadFileAsync(string bucketName, StreamForUpload fileForUpload, CancellationToken token);
        Task UploadFilesAsync(List<FileForUpload> filesForUpload, string bucketName, CancellationToken token);
        Task UploadFilesAsync(List<FileForUpload> filesForUpload, List<string> bucketNames, CancellationToken token);
        Task CopyFilesAsync(List<FileForCopy> filesForCopy);
        Task CopyFileAsync(FileForCopy fileForCopy);
    }
}
