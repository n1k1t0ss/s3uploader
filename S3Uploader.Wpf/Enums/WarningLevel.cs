﻿namespace S3Uploader.Wpf.Enums
{
    public enum WarningLevel
    {
        Usual = 0,
        Critical = 1
    }
}
