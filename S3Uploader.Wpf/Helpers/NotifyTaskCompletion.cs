﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace S3Uploader.Wpf.Helpers
{
    public sealed class NotifyTaskCompletion<TResult> : INotifyPropertyChanged
    {
        private readonly TResult _defaultResult;

        public NotifyTaskCompletion(Task<TResult> task)
        {
            Task = task;
            TaskCompletion = WatchTaskAsync(task);
            _defaultResult = default(TResult);
        }

        public NotifyTaskCompletion(Task<TResult> task, TResult defaultResult)
        {
            Task = task;
            TaskCompletion = WatchTaskAsync(task);
            _defaultResult = defaultResult;
        }

        private async Task WatchTaskAsync(Task task)
        {
            try
            {
                await task;
            }
            catch (Exception)
            {
                // ignored: catch error in UI (xaml)
            }
            var propertyChanged = PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged(this, new PropertyChangedEventArgs(nameof(Status)));
            propertyChanged(this, new PropertyChangedEventArgs(nameof(IsCompleted)));
            propertyChanged(this, new PropertyChangedEventArgs(nameof(IsNotCompleted)));
            if (task.IsCanceled)
            {
                propertyChanged(this, new PropertyChangedEventArgs(nameof(IsCanceled)));
                propertyChanged(this, new PropertyChangedEventArgs(nameof(IsCompletedNotSuccessfully)));
            }
            else if (task.IsFaulted)
            {
                propertyChanged(this, new PropertyChangedEventArgs(nameof(IsFaulted)));
                propertyChanged(this, new PropertyChangedEventArgs(nameof(Exception)));
                propertyChanged(this, new PropertyChangedEventArgs(nameof(InnerException)));
                propertyChanged(this, new PropertyChangedEventArgs(nameof(ErrorMessage)));
                propertyChanged(this, new PropertyChangedEventArgs(nameof(IsCompletedNotSuccessfully)));
            }
            else
            {
                propertyChanged(this, new PropertyChangedEventArgs(nameof(IsSuccessfullyCompleted)));
                
                propertyChanged(this, new PropertyChangedEventArgs(nameof(Result)));
            }
        }

        public Task<TResult> Task { get; }
        public Task TaskCompletion { get;  }
        public TResult Result => (Task.Status == TaskStatus.RanToCompletion) ? Task.Result : _defaultResult;

        public TaskStatus Status => Task.Status;
        public bool IsCompleted => Task.IsCompleted;
        public bool IsNotCompleted => !Task.IsCompleted;
        public bool IsSuccessfullyCompleted => Task.Status == TaskStatus.RanToCompletion;
        public bool IsCompletedNotSuccessfully => IsCanceled || IsFaulted;

        public bool IsCanceled => Task.IsCanceled;
        public bool IsFaulted => Task.IsFaulted;
        public AggregateException Exception => Task.Exception;

        public Exception InnerException => Exception?.InnerException;

        public string ErrorMessage => InnerException?.Message;
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
