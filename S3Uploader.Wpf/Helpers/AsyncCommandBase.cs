﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace S3Uploader.Wpf.Helpers
{
    public abstract class AsyncCommandBase : IAsyncCommand
    {
        internal readonly CancelAsyncCommand _cancelCommand;

        internal AsyncCommandBase()
        {
            _cancelCommand = new CancelAsyncCommand();
        }

        public abstract bool CanExecute(object parameter);

        public abstract Task ExecuteAsync(object parameter);
        public void Cancel()
        {
            CancelCommand.Execute(null);
        }

        public ICommand CancelCommand => _cancelCommand;

        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        protected void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
