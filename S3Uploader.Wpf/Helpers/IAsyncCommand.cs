﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace S3Uploader.Wpf.Helpers
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
        void Cancel();
    }
}
