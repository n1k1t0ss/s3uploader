﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace S3Uploader.Wpf.Helpers
{
    public static class AsyncCommandHelper
    {
        /// <summary>
        /// Async command with input parametr and output.
        /// </summary>
        // Usage: 
        // var command = AsyncCommandHelper.Create<objectInput, objectOutput>(FOO);
        // Foo signature:
        // async Task<objectOutput> FOO(objectInput input, CancellationToken token)
        public static AsyncCommand<TInput, TResult> Create<TInput, TResult>(Func<TInput, CancellationToken, Task<TResult>> command)
        {
            return new AsyncCommand<TInput, TResult>(command);
        }

        /// <summary>
        /// Async command with only input parametr.
        /// </summary>
        // Usage: 
        // var command = AsyncCommandHelper.Create<objectInput>(FOO);
        // Foo signature:
        // async Task FOO(objectInput input, CancellationToken token)
        public static AsyncCommand<TInput, object> Create<TInput>(Func<TInput, CancellationToken, Task> command)
        {
            return new AsyncCommand<TInput, object>(async (input, token) =>
            {
                await command(input, token);
                return null;
            });
        }

        /// <summary>
        /// Async command with only output.
        /// </summary>
        // Usage: 
        // var command = AsyncCommandHelper.Create<objectOutput>(FOO);
        // Foo signature:
        // async Task<objectOutput> FOO(CancellationToken token)
        public static AsyncCommand<TResult> Create<TResult>(Func<CancellationToken, Task<TResult>> command)
        {
            return new AsyncCommand<TResult>(command);
        }

        /// <summary>
        /// Async command with without input and output.
        /// </summary>
        // Usage: 
        // var command = AsyncCommandHelper.Create(FOO);
        // Foo signature:
        // async Task FOO(CancellationToken token)
        public static AsyncCommand<object> Create(Func<CancellationToken, Task> command)
        {
            return new AsyncCommand<object>(async (token) =>
            {
                await command(token);
                return null;
            });
        }
    }

}
