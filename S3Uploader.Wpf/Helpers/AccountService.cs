﻿using System.Collections.Generic;
using System.Linq;
using S3Uploader.Logic;
using S3Uploader.Logic.Models;
using S3Uploader.Wpf.Properties;

namespace S3Uploader.Wpf.Helpers
{
    public class AccountService : IAccountService
    {
        public List<AccountSetting> GetList()
        {
            return Settings.Default.Accounts?.Select(o => o.Copy()).ToList();
        }

        public AccountSetting GetSelectedAccount()
        {
            var settings = Settings.Default.Accounts?.SingleOrDefault(o => o.IsSelected);
            if (settings == null)
            {
                return null;
            }

            return new AccountSetting
            {
                AccessKeyId = settings.AccessKeyId,
                SecretAccessKeyId = settings.SecretAccessKeyId
            };
        }

        public void Save(List<AccountSetting> settings)
        {
            Settings.Default.Accounts = settings.Select(o => o.Copy()).ToList();
            Settings.Default.Save();
        }

        public bool IsReady()
        {
            var settings = Settings.Default.Accounts?.SingleOrDefault(o => o.IsSelected);
            return settings != null;
        }
    }
}
