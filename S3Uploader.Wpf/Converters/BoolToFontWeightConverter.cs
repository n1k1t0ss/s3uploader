﻿using System;
using System.Windows;
using System.Windows.Data;

namespace S3Uploader.Wpf.Converters
{
    [ValueConversion(typeof(bool), typeof(FontWeights))]
    public class BoolToFontWeightConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? FontWeights.Bold : FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
