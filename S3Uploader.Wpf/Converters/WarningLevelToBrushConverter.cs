﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using S3Uploader.Wpf.Enums;

namespace S3Uploader.Wpf.Converters
{
    [ValueConversion(typeof(WarningLevel), typeof(SolidColorBrush))]
    public sealed class WarningLevelToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var level = (WarningLevel)value;
            switch (level)
            {
                default:
                case WarningLevel.Usual:
                    return Brushes.Black;

                case WarningLevel.Critical:
                    return Brushes.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
