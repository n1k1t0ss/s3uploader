﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using S3Uploader.Logic;
using S3Uploader.Logic.Models;
using S3Uploader.Wpf.Helpers;

namespace S3Uploader.Wpf.ViewModels.AccountSettings
{
    public class ManageAccountsViewModel : ViewModelBase, IViewModel
    {
        public ICommand EditCommand { get; }
        public ICommand AddCommand { get; }
        public ICommand DeleteCommand { get; }
        public ICommand SelectCommand { get; }
        public ICommand SaveChangesCommand { get; }

        private readonly IMainViewModel _mainViewModel;

        private readonly IAccountService _accountService;

        private bool? _dialogResult;
        public bool? DialogResult
        {
            get => _dialogResult;
            set
            {
                _dialogResult = value;
                NotifyPropertyChanged(nameof(DialogResult));
            }
        }

        public ManageAccountsViewModel(IMainViewModel mainViewModel, IAccountService accountService)
        {
            _mainViewModel = mainViewModel;
            _accountService = accountService;

            Accounts = _accountService.GetList() == null
                ? new ObservableCollection<AccountSetting>()
                : new ObservableCollection<AccountSetting>(_accountService.GetList());

            AddCommand = new DelegateCommand(AddAccount);
            EditCommand = new DelegateCommand<AccountSetting>(EditAccount);
            DeleteCommand = new DelegateCommand<AccountSetting>(DeleteAccount);
            SelectCommand = new DelegateCommand<AccountSetting>(SelectAccount);
            SaveChangesCommand = new DelegateCommand(SaveChanges);
        }

        private void AddAccount()
        {
            var manageAccountViewModel = new ManageAccountViewModel(Accounts.ToList());
            var showDialog = _mainViewModel.ShowDialogManageAccountView(manageAccountViewModel);

            if (showDialog == true)
            {
                Accounts.Add(manageAccountViewModel.Account);
                if (Accounts.Count == 1)
                {
                    Accounts.First().IsSelected = true;
                }
            }
        }

        private void EditAccount(AccountSetting account)
        {
            var manageAccountViewModel = new ManageAccountViewModel(Accounts.ToList(), account);
            var showDialog = _mainViewModel.ShowDialogManageAccountView(manageAccountViewModel);

            if (showDialog == true)
            {
                account.AccountName = manageAccountViewModel.Account.AccountName;
                account.AccessKeyId = manageAccountViewModel.Account.AccessKeyId;
                account.Type = manageAccountViewModel.Account.Type;
                account.SecretAccessKeyId = manageAccountViewModel.Account.SecretAccessKeyId;

                Accounts = new ObservableCollection<AccountSetting>(Accounts.ToList());
            }
        }

        private void SaveChanges()
        {
            _accountService.Save(Accounts.ToList());
            DialogResult = true;
        }

        private void SelectAccount(AccountSetting account)
        {
            foreach (var acc in Accounts)
            {
                acc.IsSelected = false;
            }
            Accounts.Single(o => o.AccountName == account.AccountName).IsSelected = true;
            Accounts = new ObservableCollection<AccountSetting>(Accounts.ToList());
        }


        private void DeleteAccount(AccountSetting account)
        {
            Accounts.Remove(account);
            Accounts = new ObservableCollection<AccountSetting>(Accounts.ToList());
        }


        private ObservableCollection<AccountSetting> _accounts;
        public ObservableCollection<AccountSetting> Accounts
        {
            get => _accounts;
            set
            {
                _accounts = value;
                NotifyPropertyChanged(nameof(Accounts));
            }
        }


        private string _errorText;
        public string ErrorText
        {
            get => _errorText;
            set
            {
                _errorText = value;
                NotifyPropertyChanged(nameof(ErrorText));
            }
        }

        private string _isErrorTextVisible;
        public string IsErrorTextVisible
        {
            get => _isErrorTextVisible;
            set
            {
                _isErrorTextVisible = value;
                NotifyPropertyChanged(nameof(IsErrorTextVisible));
            }
        }


        public void Dispose()
        {
        }
    }
}
