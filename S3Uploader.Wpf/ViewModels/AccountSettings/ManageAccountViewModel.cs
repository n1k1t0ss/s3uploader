﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using S3Uploader.Logic.Enums;
using S3Uploader.Logic.Models;
using S3Uploader.Wpf.Helpers;

namespace S3Uploader.Wpf.ViewModels.AccountSettings
{
    public class ManageAccountViewModel : ViewModelBase, IViewModel
    {
        private readonly List<AccountSetting> _allAccounts;
        private readonly AccountSetting _originalAccount;

        public ICommand SaveCommand { get; set; }


        public ManageAccountViewModel(List<AccountSetting> allAccounts) 
            :this (allAccounts, null)
        {
        }

        public ManageAccountViewModel(List<AccountSetting> allAccounts, AccountSetting account)
        {
            _allAccounts = allAccounts;
            
            if (account != null)
            {
                _originalAccount = account;
                Account = new AccountSetting()
                {
                    AccountName = account.AccountName,
                    AccessKeyId = account.AccessKeyId,
                    SecretAccessKeyId = account.SecretAccessKeyId,
                };
            }
            else
            {
                Account = new AccountSetting();
            }

            SaveCommand = new DelegateCommand(Save);
        }

        private void Save()
        {
            if(((_originalAccount != null && Account.AccountName != _originalAccount.AccountName) || (_originalAccount == null)) 
                && _allAccounts.Any(o => o?.AccountName == Account.AccountName))
            {
                ErrorText = "Account name should be unique!";
                return;
            }

            DialogResult = true;
        }

        private bool? _dialogResult;
        public bool? DialogResult
        {
            get => _dialogResult;
            set
            {
                _dialogResult = value;
                NotifyPropertyChanged(nameof(DialogResult));
            }
        }
        private AccountSetting _account;
        public AccountSetting Account
        {
            get => _account;
            set
            {
                _account = value;
                NotifyPropertyChanged(nameof(Account));
            }
        }

        private string _errorText;
        public string ErrorText
        {
            get => _errorText;
            set
            {
                _errorText = value;
                ErrorTextIsVisible = !string.IsNullOrEmpty(ErrorText);
                NotifyPropertyChanged(nameof(ErrorText));
            }
        }

        private bool _errorTextIsVisible;
        public bool ErrorTextIsVisible
        {
            get => _errorTextIsVisible;
            set
            {
                _errorTextIsVisible = value;
                NotifyPropertyChanged(nameof(ErrorTextIsVisible));
            }
        }

        public List<AccountSettingType> AccountTypes => Enum.GetValues(typeof(AccountSettingType)).Cast<AccountSettingType>().ToList();


        public void Dispose()
        {
        }
    }
}
