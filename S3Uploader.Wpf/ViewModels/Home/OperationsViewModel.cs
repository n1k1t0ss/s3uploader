﻿using S3Uploader.Wpf.Helpers;

namespace S3Uploader.Wpf.ViewModels.Home
{
    public sealed class OperationsViewModel
    {
        public OperationsViewModel(string taskName, AsyncCommand<object> command)
        {
            TaskName = taskName;
            Command = command;
        }

        public string TaskName { get; }

        public AsyncCommand<object> Command { get; }
    }

}
