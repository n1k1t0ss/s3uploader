﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using S3Uploader.Logic.Models;
using S3Uploader.Wpf.Helpers;

namespace S3Uploader.Wpf.ViewModels.Home
{
    public class UploadViewModel : ViewModelBase, IViewModel
    {
        public readonly UploadModel Model;

        public UploadViewModel(List<string> uploadBuckets, string path)
        {
            Model = new UploadModel();
            UploadBuckets = uploadBuckets;
            UploadPath = path;
            OpenCommand = new DelegateCommand(OpenFile);
            UploadCommand = new DelegateCommand(Upload);
        }
        private bool? _dialogResult;
        public bool? DialogResult
        {
            get => _dialogResult;
            set
            {
                _dialogResult = value;
                NotifyPropertyChanged(nameof(DialogResult));
            }
        }

        public ICommand OpenCommand { get; set; }
        public ICommand UploadCommand { get; set; }

        private void Upload()
        {
            DialogResult = true;
        }

        private void OpenFile()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = "",
                Multiselect = true
            };
            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                SelectedFilePaths = dlg.FileNames.ToList();
            }
        }

        public string UploadPath
        {
            get => Model.UploadPath;
            set
            {
                Model.UploadPath = value;
                NotifyPropertyChanged(nameof(UploadPath));
            }
        }

        public List<string> UploadBuckets
        {
            get => Model.UploadBuckets;
            set
            {
                Model.UploadBuckets = value;
                NotifyPropertyChanged(nameof(UploadBuckets));
            }
        }

        public List<string> SelectedFilePaths
        {
            get => Model.SelectedFilePaths;
            set
            {
                Model.SelectedFilePaths = value;
                NotifyPropertyChanged(nameof(SelectedFilePaths));
            }
        }
        public bool SendInvalidationRequest
        {
            get => Model.SendInvalidationRequest;
            set
            {
                Model.SendInvalidationRequest = value;
                NotifyPropertyChanged(nameof(SendInvalidationRequest));
            }
        }

        public bool IsPublic
        {
            get => Model.IsPublic;
            set
            {
                Model.IsPublic = value;
                NotifyPropertyChanged(nameof(IsPublic));
            }
        }

        public bool UseSsl
        {
            get => Model.UseSsl;
            set
            {
                Model.UseSsl = value;
                NotifyPropertyChanged(nameof(UseSsl));
            }
        }

        public bool NoneCriticalData
        {
            get => Model.NoneCriticalData;
            set
            {
                Model.NoneCriticalData = value;
                NotifyPropertyChanged(nameof(NoneCriticalData));
            }
        }


        public void Dispose()
        {
        }
    }
}
