﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using S3Uploader.Logic;
using S3Uploader.Logic.Models;
using S3Uploader.Wpf.Enums;
using S3Uploader.Wpf.Helpers;
using S3Uploader.Wpf.ViewModels.AccountSettings;

namespace S3Uploader.Wpf.ViewModels.Home
{
    public class HomeViewModel : ViewModelBase, IViewModel
    {
        private readonly IMainViewModel _mainViewModel;

        public IAsyncCommand LoadBucketsCommand { get; }
        public IAsyncCommand LoadFilesCommand { get; }

        public ICommand RefreshBucketsCommand { get; }

        public ICommand FilterFoldersCommand { get; }
        public ICommand UploadCommand { get; }
        public ICommand ManageAccountsCommand { get; }

        private readonly IAccountService _accountService;
        private readonly IFileService _fileService;

        public HomeViewModel(IMainViewModel mainViewModel, IAccountService accountService, IFileService fileService)
        {
            _mainViewModel = mainViewModel;
            _accountService = accountService;
            _fileService = fileService;

            LoadBucketsCommand = AsyncCommandHelper.Create(LoadBucketsAsync);
            LoadFilesCommand = AsyncCommandHelper.Create<string>(LoadFilesAsync);

            RefreshBucketsCommand = new DelegateCommand(RefreshBuckets);
            FilterFoldersCommand = new DelegateCommand(FilterFolders);
            UploadCommand = new DelegateCommand(OpenUploadWindow);
            ManageAccountsCommand = new DelegateCommand(OpenManageAccountsWindow);
            Operations = new ObservableCollection<OperationsViewModel>();

            if (_accountService.IsReady())
            {
                RefreshBucketsCommand.Execute(null);
            }
        }

        public async Task LoadBucketsAsync(CancellationToken token)
        {
            var bucketsTask = _fileService.GetBucketListAsync(token);
            if (string.IsNullOrEmpty(FoldersFilter))
            {
                Buckets = new NotifyTaskCompletion<List<string>>(bucketsTask);
                await bucketsTask;
            }
            else
            {
                // filter buckets - get only that had a file
                var buckets = await bucketsTask;
                var bucketsFilteredTask = _fileService.IsPathsExistsAsync(buckets, BucketFoldersFilter, token);
                Buckets = new NotifyTaskCompletion<List<string>>(bucketsFilteredTask);
                await bucketsFilteredTask;
            }
        }

        public async Task LoadFilesAsync(string selectedBucketName, CancellationToken token)
        {
            var history = new List<FileList>();

           var task = _fileService.GetFileListAsync(selectedBucketName, BucketFoldersFilter, 
               ((taskFiles, isTruncated) =>
                {
                    token.ThrowIfCancellationRequested();
                    // dynamically loading list of elements
                    Files = new NotifyTaskCompletion<List<FileList>>(AddRangeToListAsync(taskFiles, history), history.ToList());
                }), token
            );
           await task;
        }
        private async Task<List<FileList>> AddRangeToListAsync(Task<List<FileList>> task, List<FileList> list)
        {
            list.AddRange(await task.ConfigureAwait(false));
            return list;
        }



        private void RefreshBuckets()
        {
            LoadBucketsCommand.Cancel();
            LoadFilesCommand.Cancel();

            FoldersFilter = null;
            BucketFoldersFilter = null;
            
            LoadBucketsCommand.Execute(null);
        }

        private void OpenUploadWindow()
        {
            if (string.IsNullOrEmpty(BucketFoldersFilter))
            {
                var result = MessageBox.Show("You didn't fill the filterbox. Do you really want to upload file(s) to ALL buckets?", "WARNING",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result != MessageBoxResult.Yes)
                {
                    return;
                }
            }
            
            var viewModel = new UploadViewModel(Buckets.Result, BucketFoldersFilter);

            if (_mainViewModel.ShowDialogUploadView(viewModel) == true)
            {
                var uploadCommand = AsyncCommandHelper.Create(token => _fileService.UploadAsync(viewModel.Model, token));
                uploadCommand.Execute(null);
                Operations.Add(new OperationsViewModel("Upload file", uploadCommand));
            }
        }

        public ObservableCollection<OperationsViewModel> Operations { get; }

        public void OpenManageAccountsWindow()
        {
            var viewModel = new ManageAccountsViewModel(_mainViewModel, _accountService);
            _mainViewModel.ShowDialogManageAccountsView(viewModel);
            
            RefreshBucketsCommand.Execute(null);
        }
        
        private void FilterFolders()
        {
            LoadBucketsCommand.Cancel();
            LoadFilesCommand.Cancel();

            BucketFoldersFilter = FoldersFilter;
            LoadBucketsCommand.Execute(null);
        }


        private NotifyTaskCompletion<List<string>> _buckets;

        public NotifyTaskCompletion<List<string>> Buckets
        {
            get => _buckets;
            set
            {
                _buckets = value;
                NotifyPropertyChanged(nameof(Buckets));
            }
        }


        private NotifyTaskCompletion<List<FileList>> _files;
        public NotifyTaskCompletion<List<FileList>> Files
        {
            get => _files;
            set
            {
                _files = value;
                NotifyPropertyChanged(nameof(Files));
            }
        }

        private NotifyTaskCompletion<Task> _fileLoading;
        public NotifyTaskCompletion<Task> FileLoading
        {
            get => _fileLoading;
            set
            {
                _fileLoading = value;
                NotifyPropertyChanged(nameof(FileLoading));
            }
        }


        /// <summary>
        /// Representation of FolderFilter textbox
        /// </summary>
        private string _foldersFilter;
        public string FoldersFilter
        {
            get => _foldersFilter;
            set
            {
                _foldersFilter = value;
                NotifyPropertyChanged(nameof(FoldersFilter));
                NotifyPropertyChanged(nameof(FoldersFilterForegroundColor));
                NotifyPropertyChanged(nameof(IsReadyForUpload));
                NotifyPropertyChanged(nameof(IsNotReadyForUpload));
            }
        }

        /// <summary>
        /// Filter that synchronized with s3
        /// </summary>
        private string _bucketFoldersFilter;
        public string BucketFoldersFilter
        {
            get => _bucketFoldersFilter;
            set
            {
                _bucketFoldersFilter = value;
                NotifyPropertyChanged(nameof(BucketFoldersFilter));
                NotifyPropertyChanged(nameof(FoldersFilterForegroundColor));
                NotifyPropertyChanged(nameof(IsReadyForUpload));
                NotifyPropertyChanged(nameof(IsNotReadyForUpload));
            }
        }

        public WarningLevel FoldersFilterForegroundColor => FoldersFilter == BucketFoldersFilter ? WarningLevel.Usual : WarningLevel.Critical;

        public bool IsReadyForUpload
        {
            get
            {
                if (FoldersFilter != BucketFoldersFilter)
                    return false;

                return string.IsNullOrEmpty(FoldersFilter) || FoldersFilter.EndsWith("/");
            }
        }

        public bool IsNotReadyForUpload => !IsReadyForUpload;

        public string SelectBucket
        {
            set
            {
                LoadFilesCommand.Cancel();
                LoadFilesCommand.Execute(value);
            }
        }



        public void Dispose()
        {
        }
    }
}
