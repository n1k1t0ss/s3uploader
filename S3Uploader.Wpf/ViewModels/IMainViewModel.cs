﻿using System;
using S3Uploader.Wpf.ViewModels.AccountSettings;
using S3Uploader.Wpf.ViewModels.Home;

namespace S3Uploader.Wpf.ViewModels
{
    public interface IMainViewModel : IDisposable
    {

        bool? ShowDialogManageAccountView(ManageAccountViewModel viewModel);

        void ShowDialogManageAccountsView(ManageAccountsViewModel viewModel);

        bool? ShowDialogUploadView(UploadViewModel viewModel);
    }
}
