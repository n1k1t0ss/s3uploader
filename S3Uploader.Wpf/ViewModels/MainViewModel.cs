﻿using S3Uploader.Logic;
using S3Uploader.Wpf.ViewModels.AccountSettings;
using S3Uploader.Wpf.ViewModels.Home;

namespace S3Uploader.Wpf.ViewModels
{
    public class MainViewModel : ViewModelBase, IMainViewModel
    {

        public IViewManager ViewManager { get; set; }


        public MainViewModel(IViewManager viewManager, IAccountService accountService, IFileService fileService)
        {
            ViewManager = viewManager;
            
            var homeViewModel = new HomeViewModel(this, accountService, fileService);
            ViewManager.ShowHomeView(homeViewModel);

            if (!accountService.IsReady())
                homeViewModel.OpenManageAccountsWindow();
        }

        public bool? ShowDialogManageAccountView(ManageAccountViewModel viewModel)
        {
            return ViewManager.ShowDialogManageAccountView(viewModel);
        }

        public void ShowDialogManageAccountsView(ManageAccountsViewModel viewModel)
        {
            ViewManager.ShowDialogManageAccountsView(viewModel);
        }

        public bool? ShowDialogUploadView(UploadViewModel viewModel)
        {
            return ViewManager.ShowDialogUploadView(viewModel);
        }

        public void Dispose()
        {
            ViewManager?.Dispose();
        }
    }
}
