﻿using System;

namespace S3Uploader.Wpf.ViewModels
{
    public interface IViewManager : IDisposable
    {
        bool? ShowDialogManageAccountView(IViewModel viewModel);

        void ShowHomeView(IViewModel viewModel);
        bool? ShowDialogManageAccountsView(IViewModel viewModel);
        bool? ShowDialogUploadView(IViewModel viewModel);
    }
}
