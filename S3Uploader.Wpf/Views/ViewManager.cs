﻿using System;
using S3Uploader.Wpf.ViewModels;
using S3Uploader.Wpf.Views.AccountSettings;
using S3Uploader.Wpf.Views.Home;

namespace S3Uploader.Wpf.Views
{
    public class ViewManager : IViewManager
    {

        public ViewManager()
        {
        }

        public bool? ShowDialogManageAccountView(IViewModel viewModel)
        {
            var view = new ManageAccountView
            {
                DataContext = viewModel
            };

            return view.ShowDialog();
        }

        private EventHandler _homeViewClosedEventHandler;
        private HomeView _homeView;

        public void ShowHomeView(IViewModel viewModel)
        {
            _homeView = new HomeView()
            {
                DataContext = viewModel
            };
            _homeView.Show();

            _homeViewClosedEventHandler = (sender, args) =>
            {
                viewModel.Dispose();
            };

            _homeView.Closed += _homeViewClosedEventHandler;
        }

        public void CloseHomeView()
        {
            _homeView.Closed -= _homeViewClosedEventHandler;
            _homeView?.Close();
        }


        public bool? ShowDialogManageAccountsView(IViewModel viewModel)
        {
            var manageAccountsView = new ManageAccountsView()
            {
                DataContext = viewModel
            };

            return manageAccountsView.ShowDialog();
        }



        private EventHandler _uploadViewClosedEventHandler;
        private UploadView _uploadView;

        public bool? ShowDialogUploadView(IViewModel viewModel)
        {
            _uploadView = new UploadView()
            {
                DataContext = viewModel
            };

            _uploadViewClosedEventHandler = (sender, args) =>
            {
                viewModel.Dispose();
            };


            _uploadView.Closed += _uploadViewClosedEventHandler;

            return _uploadView.ShowDialog();
        }

        public void UploadViewClose()
        {
            _uploadView.Closed -= _uploadViewClosedEventHandler;
            _uploadView?.Close();
        }

        public void Dispose()
        {
        }
    }
}
