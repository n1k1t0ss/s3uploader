﻿using System.Windows;

namespace S3Uploader.Wpf.Views.AccountSettings
{
    /// <summary>
    /// Interaction logic for ManageAccountView.xaml
    /// </summary>
    public partial class ManageAccountView : Window
    {

        public ManageAccountView()
        {
            InitializeComponent();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }

    }
}
