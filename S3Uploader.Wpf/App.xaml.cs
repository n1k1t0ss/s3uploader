﻿using System.Windows;
using AmazonStorage.Implementation.Factory;
using S3Uploader.Logic;
using S3Uploader.Wpf.Helpers;
using S3Uploader.Wpf.ViewModels;
using S3Uploader.Wpf.Views;

namespace S3Uploader.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var viewManager = new ViewManager();
            
            var accountService = new AccountService();
            var amazonStorageFactory = new AmazonStorageFactory();
            var fileService = new FileService(amazonStorageFactory, accountService);

            var mainView = new MainViewModel(viewManager, accountService, fileService);
        }
    }
}
